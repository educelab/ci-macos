# Updating a runner

## Update macOS system and software
```shell
# List available updates
softwareupdate -l

# Update macOS. Select a label returned by the above command. Be sure 
# to select an OS update that matches the runner's configuration. For 
# example, at the time of this writing, avocado runners are pinned to 
# macOS 12 (Monterey).
sudo softwareupdate -iR 'macOS Monterey 12.7.2-21G1974'

# Update system software. Add other update labels as needed.
softwareupdate -i 'Command Line Tools for Xcode-14.2' 
```

## Update vc-deps
To build against vc-deps tagged version `v1.8.1`:

```shell
cd ~/source/ci-macos/
sudo bash
export VCDEPS_VERSION="1.8.1"
./install-vcdeps.sh  
```

To build against a different vc-deps branch or tag:

```shell
cd ~/source/ci-macos/
sudo bash
export VCDEPS_VERSION="dirty"
export VCDEPS_REF="develop"
./install-vcdeps.sh  
```

## Update Homebrew packages
```shell
brew upgrade
cd ci-macos/
brew bundle
```

## Updating Qt installations
```shell
cd /usr/local/Qt/MaintenanceTool.app/Contents/MacOS/

# Check for updates
sudo ./MaintenanceTool check-updates

# Update existing packages 
# You may have to run this multiple times if MaintenanceTool gets updated
sudo ./MaintenanceTool update

# Install a new version of Qt
QT_VERSION=642  # 6.4.2
sudo ./MaintenanceTool install \
  "qt.qt6.${QT_VERSION}.clang_64" \
  "qt.qt6.${QT_VERSION}.addons.qt3d" \
  "qt.qt6.${QT_VERSION}.addons.qtcharts" \
  "qt.qt6.${QT_VERSION}.addons.qtdatavis3d" \
  "qt.qt6.${QT_VERSION}.addons.qtimageformats" \
  "qt.qt6.${QT_VERSION}.doc" \
  "qt.qt6.${QT_VERSION}.doc.qt3d" \
  "qt.qt6.${QT_VERSION}.doc.qtcharts" \
  "qt.qt6.${QT_VERSION}.doc.qtdatavis3d" \
  "qt.qt6.${QT_VERSION}.doc.qtimageformats" \
  "qt.qt6.${QT_VERSION}.examples" \
  "qt.qt6.${QT_VERSION}.examples.qt3d" \
  "qt.qt6.${QT_VERSION}.examples.qtcharts" \
  "qt.qt6.${QT_VERSION}.examples.qtdatavis3d" \
  "qt.qt6.${QT_VERSION}.examples.qtimageformats"
```

## Updating the GitLab Runner service

- Login to the `gitlab-runner` account and stop the service:
  ```shell
  su - gitlab-runner
  gitlab-runner stop
  exit
  ```

- Under the administrator account, download [the latest GitLab runner binary](https://docs.gitlab.com/runner/install/osx.html#manual-update) to the administrator's home directory:
  ```shell
  # Intel-based systems
  curl -o ~/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64"
  
  # Apple Silicon-based systems
  curl -o ~/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-arm64"
  ```

- Give the binary executable privileges and move it to the globally readable binary directory:
  ```shell
  sudo chmod +x ~/gitlab-runner
  sudo mv ~/gitlab-runner /usr/local/bin
  ```

- Login to the `gitlab-runner` account and restart the service:
  ```shell
  su - gitlab-runner
  gitlab-runner start
  ```

## FAQs
### `Authentication failed` when running the Qt `MaintenanceTool`
```
read "QT_EMAIL?Enter Qt account e-mail: "
read -s "QT_PASS?Enter Qt account password: "
sudo ./MaintenanceTool -m ${QT_EMAIL} --pw ${QT_PASS} check-updates
```

### After updating GitLab Runner, `gitlab-runner start` fails with the error `zsh: killed`
Newer versions of macOS include a security feature for auto-quarantining files 
which have been downloaded from the internet. To keep the OS from quarantining the 
updated `gitlab-runner` exectuable, first download it to your home directory
and then move it to `/usr/local/bin`:
```bash
curl --output ~/gitlab-runner "https://s3.amazonaws.com/gitlab-runner-downloads/latest/binaries/gitlab-runner-darwin-arm64"
sudo chmod +x ~/gitlab-runner
sudo mv ~/gitlab-runner /usr/local/bin/gitlab-runner
```

See [this issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29080) for more details.