# Runner configurations

EduceLab macOS runners are named according to the hardware configurations.
This document lists and describes the available configurations.

## avocado
- Device: Mac mini (M1, 2020)
- OS: macOS Monterey
- Memory: 16GB
- Storage: 250GB
- User logo: Emojis -> Avocado
- GitLab Runner tags: `avocado,macos,build,test,deploy,lint,source`
- Concurrent jobs: 1
