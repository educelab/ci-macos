# Setting up a new runner
To maintain consistency across devices, all macOS runners are configured
using the following instructions. As is the case with all Apple devices, 
software support is closely tied to the hardware. Refer to the 
[configurations list](configurations.md) for documented discrepancies 
between these instructions and the actual configurations.

[toc]

## Setup the device
Proceed through the macOS setup wizard, selecting the following options:
- Language: English
- Country or Region: United States
- Accessibility Options: Not Now
- Migration Assistant: Not Not
- Apple ID: Set Up Later (Skip to confirm)
- Terms and Conditions: Agree
- Computer Account:
  - Full name: Admin
  - Account name: admin
  - Logo: Set according to the named configuration
  - Password: Refer to the password policy to generate a new password
  - Hint: Leave blank
- Enable Location Services: On
- Analytics:
  - Shared Mac Analytics with Apple: Off
  - Share crash and usage data with app developers: Off
- Screen Time: Set Up Later
- Choose Your Look: Dark

Once you are taken to the macOS desktop, finish customization and
apply system updates. Open System Preferences, then complete the 
following:
- Energy Saver
  - Prevent your Mac from automatically sleeping when the display is off: On
  - Put hard disks to sleep when possible: On
  - Wake for network access: On
  - Start up automatically after a power failure: On
- Sharing
  - Computer name: Change to form `config.#` (e.g. `avocado.1`). See 
  the [configurations list](configurations.md) for a list of configuration names. 
  - Disable all services
  - Enable Remote Login
    - Only these users: Administrators
- Software Update:
  - Apply all listed updates

## Install Xcode
Open the macOS App Store and login using the shared developer account.
Search for and install Xcode. After the installation is complete, open 
Terminal and install the Xcode Command Line Utilities:
```shell
xcode-select --install
```
After the Command Line Utilities finish installing, ensure the developer
toolchain is still properly configured and accept the Xcode license:
```shell
sudo xcode-select -s /Applications/Xcode.app/
sudo xcodebuild -license
```

Go to System Preferences -> Software Update and make sure the system is 
still up-to-date.

## Setup this repository
First, setup a directory in the Admin user directory for code that will 
be installed from source:
```shell
# Setup local source directory
mkdir -p ~/source
cd ~/source

# Clone the ci-macos configuration repository
git clone https://gitlab.com/educelab/ci-macos.git
```

## Install Homebrew dependencies
Run the following command in Terminal to install Homebrew: 
```shell
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Once installation is complete, add Homebrew to PATH:
```shell
echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.zprofile
eval "$(/opt/homebrew/bin/brew shellenv)"
```

Finally, install the default Homebrew packages:
```shell
cd ~/source/ci-macos
brew bundle
```

# Install vc-deps configurations
Setup a publicly readable installation location:
```shell
sudo mkdir -p /usr/local/educelab
sudo chmod +rx /usr/local/educelab
```

Run the included vc-deps installation script:
```shell
/bin/bash ~/source/ci-macos/install-vcdeps.sh
```

## Install Qt using the online installer
Navigate to the [Qt Open Source download site](https://www.qt.io/download-open-source) and 
download the Qt Online Installer for macOS. Open the downloaded DMG and run the installer 
application.

Login to the Qt developer account.

Set the following installation options when prompted:
- Contribute to Qt Development
  - Disable sending pseudonymous usage statistics in Qt Creator
- Installation Folder
  - Installation directory: `/usr/local/Qt`
  - Custom installation
- Select Components (Select the latest version and any desired alternates)
  - Qt 6.x.x 
    - macOS
    - Additional Libraries
      - Qt 3D 
      - Qt Charts
      - Qt Data Visualization
    - Qt Debug Information Files
  - Qt 5.15.2
    - macOS
    - Qt Debug Information Files
  - Developer and Designer Tools
    - Uncheck CMake x.x.x
    - Uncheck Ninja x.x.x

## Setup GitLab Runner
- Setup a publicly readable binary installation location:
  ```shell
  sudo mkdir -p /usr/local/bin/
  sudo chmod +rx /usr/local/bin/
  ```

- Follow [the official instructions](https://docs.gitlab.com/runner/install/osx.html#manual-installation-official) for 
manually installing GitLab Runner on macOS. **Ignore any instructions for registering the runner.**

- Add an unprivileged user account for running jobs.
  - System Preferences -> Users and Groups
    - Unlock the settings pane
    - Click the + sign at the bottom of the users list
    - Account info:
      - New Account: Standard
      - Full Name: GitLab Runner
      - Account Name: gitlab-runner
      - Refer to the password policy to generate a new password
    - Change the user icon according to the named configuration
    - Select Login Options at the borrom of the user list
    - Automatic login: GitLab Runner
    - Lock the settings pane

- Login to the new account. When logging in for the first time, configure the 
  account with the following settings:
  - Sign In with Your Apple ID: Set Up Later
  - Screen Time: Set Up Later
  - Choose Your Look: Dark

- Open Terminal and add the Homebrew path to the zsh and bash profiles:
  ```shell
  echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.zprofile
  echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.bash_profile
  eval "$(/opt/homebrew/bin/brew shellenv)"
  ```

- Register the runner with the GitLab instance:
  ```shell
  gitlab-runner register
  ```
  - GitLab instance URL: Get from the server
  - Registration token: Get from the server
  - Description: Runner norm in form `config.#` (e.g. `avocado.1`)
  - Enter tags for the runner: See the [configurations list](configurations.md) 
  for a list of tags for each configuration.
  - Maintenance note: Leave blank.
  - Enter an executor: shell

- Install the runner service:
  ```shell
  gitlab-runner install
  ```
- Open the runner service `.plist` file with your editor of choice:
  ```shell
  vi ~/Library/LaunchAgents/gitlab-runner.plist
  ```

  Change the following lines so the runner logs are saved in a writable location:
  ```xml
  <key>StandardOutPath</key>
  <string>/Users/gitlab-runner/.gitlab-runner/gitlab-runner.out.log</string>
  <key>StandardErrorPath</key>
  <string>/Users/gitlab-runner/.gitlab-runner/gitlab-runner.err.log</string>
  ```

- Edit the top-level `concurrent` field in `~/.gitlab-runner/config.toml` to match the 
  number of concurrent jobs specified in the [configurations list](configurations.md) 
  for the given configuration.

- Start the runner service:
  ```shell
  gitlab-runner start
  gitlab-runner status
  ```
