#!/bin/bash

set -e

# Variables
export VCDEPS_VERSION=${VCDEPS_VERSION:-"1.9.0"}
export VCDEPS_REF=${VCDEPS_REF:-"v${VCDEPS_VERSION}"}
REPO=https://github.com/educelab/vc-deps.git
BASE_DIR="${HOME}/source/vc-deps-${VCDEPS_REF}"
CONFIG_DIR="${HOME}/source/ci-macos/cmake"

# Clone vc-deps
git clone "${REPO}" "${BASE_DIR}/"
git -C "${BASE_DIR}/" checkout "${VCDEPS_REF}"

# static, debug
echo "Installing vc-deps ${VCDEPS_REF} (static, debug)"
BUILD_DIR="${BASE_DIR}/build-${VCDEPS_VERSION}-static-debug/"
mkdir -p "${BUILD_DIR}"
cmake -S "${BASE_DIR}" -B "${BUILD_DIR}" -C "${CONFIG_DIR}/vcdeps-static-debug.cmake" -GNinja
sudo cmake --build "${BUILD_DIR}"
echo

# static, release
echo "Installing vc-deps ${VCDEPS_REF} (static, release)"
BUILD_DIR="${BASE_DIR}/build-${VCDEPS_VERSION}-static-release/"
mkdir -p "${BUILD_DIR}"
cmake -S "${BASE_DIR}" -B "${BUILD_DIR}" -C "${CONFIG_DIR}/vcdeps-static-release.cmake" -GNinja
sudo cmake --build "${BUILD_DIR}"
echo

# dynamic, debug
echo "Installing vc-deps ${VCDEPS_REF} (dynamic, debug)"
BUILD_DIR="${BASE_DIR}/build-${VCDEPS_VERSION}-dynamic-debug/"
mkdir -p "${BUILD_DIR}"
cmake -S "${BASE_DIR}" -B "${BUILD_DIR}" -C "${CONFIG_DIR}/vcdeps-dynamic-debug.cmake" -GNinja
sudo cmake --build "${BUILD_DIR}"
echo

# dynamic, release
echo "Installing vc-deps ${VCDEPS_REF} (dynamic, release)"
BUILD_DIR="${BASE_DIR}/build-${VCDEPS_VERSION}-dynamic-release/"
mkdir -p "${BUILD_DIR}"
cmake -S "${BASE_DIR}" -B "${BUILD_DIR}" -C "${CONFIG_DIR}/vcdeps-dynamic-release.cmake" -GNinja
sudo cmake --build "${BUILD_DIR}"
echo
