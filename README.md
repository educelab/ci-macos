# macOS CI Configuration

Configuration documents for EduceLab macOS CI runners.

## Software
### Developer tools
- Xcode
- Xcode Command Line Utilities
- CMake
- Ninja build system
- clang-format
- rclone

### vc-deps

Various precompiled configurations of the vc-deps project can be found in `/usr/local/educelab/`. Use the following pattern to select your configuration of choice:
```
vc-deps-[version]-[static|dynamic]-[release|debug]
```

For example, to compile against a static, optimized version of vc-deps 1.7, provide the following flag to CMake:
```
-DCMAKE_PREFIX_PATH=/usr/local/educelab/vc-deps-1.7-static-release/
```

### Qt

Qt is installed using the official online installer. Libraries are further
organized by version. Installation paths are:
- Qt 6.6.1: `/usr/local/Qt/6.6.1/macos/`
- Qt 6.4.2: `/usr/local/Qt/6.4.2/macos/`
- Qt 6.3.0: `/usr/local/Qt/6.3.0/macos/`
- Qt 5.15.2: `/usr/local/Qt/5.15.2/clang_64/`

## Hardware

See the [hardware configurations](configurations.md) list for a description of
the runner hardware.

## Setting up a new runner

Follow [these instructions](setup.md) to setup a new macOS runner.
