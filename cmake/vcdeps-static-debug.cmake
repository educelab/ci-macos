if(DEFINED ENV{VCDEPS_VERSION})
  set(VER $ENV{VCDEPS_VERSION})
else()
  set(VER "Dirty")
endif()

set(BUILD_SHARED_LIBS OFF CACHE BOOL "")
set(CMAKE_BUILD_TYPE "Debug" CACHE STRING "")
set(CMAKE_INSTALL_PREFIX "/usr/local/educelab/vc-deps-${VER}-static-debug" CACHE PATH "")
